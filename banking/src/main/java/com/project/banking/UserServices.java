package com.project.banking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.banking.controller.ResourceNotFoundException;
import com.project.banking.controller.UserController;
import com.project.banking.model.User;
import com.project.banking.repository.UserRepository;

@RestController
public class UserServices {
	
	@Autowired
	private UserRepository userRepository;
	
	private UserController userController = new UserController();

	
	@RequestMapping( value = "/signup")
	  public ResponseEntity<User> getUserByUsernameAndPassword(@RequestParam String username, @RequestParam String password)		
	  	throws ResourceNotFoundException {
	  	
		
		User user = this.userController.getUserForTransaction(username,password,userRepository);
	  	
	      return ResponseEntity.ok().body(user);
	  }
}
