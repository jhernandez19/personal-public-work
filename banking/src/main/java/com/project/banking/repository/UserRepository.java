package com.project.banking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.banking.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
	public User findByUsername(@Param("username") String username);
	
	public User findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
	

}