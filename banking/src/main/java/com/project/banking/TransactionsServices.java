package com.project.banking;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.project.banking.ValidationsAndConstants;
import com.project.banking.controller.ResourceNotFoundException;
import com.project.banking.controller.TransactionController;
import com.project.banking.controller.UserController;
import com.project.banking.model.Transaction;
import com.project.banking.model.User;
import com.project.banking.repository.TransactionRepository;
import com.project.banking.repository.UserRepository;

@RestController
public class TransactionsServices {
	
	private TransactionController transactionController = new TransactionController();
	private UserController userController = new UserController();
	
	@Autowired
    private TransactionRepository transactionRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	Logger logger = LoggerFactory.getLogger(TransactionsServices.class);

	@PutMapping("/deposit")
	public @ResponseBody String deposit(@RequestParam String username,
										@RequestParam String password,
										@RequestParam long amount,
										@RequestParam String description) throws ResourceNotFoundException {
		
		User user = this.userController.getUserForTransaction(username, password, this.userRepository);
		
		if (user == null)
			return ValidationsAndConstants.ERROR_MESSAGE_USER_NOT_EXIST;
		
		Transaction transaction = this.transactionController.createTransaction(user, ValidationsAndConstants.TRANSACTION_TYPE_DEPOSIT, amount, description, this.userRepository, this.transactionRepository);
		
		if (transaction ==  null)
			return ValidationsAndConstants.ERROR_MESSAGE_TRANSACTION;
		
		return transaction.toString() ;
		
	}
	
	@PutMapping("/withdraw")
	public @ResponseBody String withdraw(@RequestParam String username,
										 @RequestParam String password,
										 @RequestParam long amount,
										 @RequestParam String description) throws ResourceNotFoundException {
		
		User user = this.userController.getUserForTransaction(username, password, this.userRepository);
		
		if (user == null)
			return ValidationsAndConstants.ERROR_MESSAGE_USER_NOT_EXIST;
		
		String error = ValidationsAndConstants.isValidTransaction(amount);
		if (error != null)
			return error;
		
		Transaction transaction = this.transactionController.createTransaction(user, ValidationsAndConstants.TRANSACTION_TYPE_WITHDRAW, amount, description, this.userRepository, this.transactionRepository);
		
		if (transaction ==  null)
			return ValidationsAndConstants.ERROR_MESSAGE_TRANSACTION;
		
		return transaction.toString() ;
		 
		
	}
	
	@RequestMapping("/user/transactions")
	public @ResponseBody String checkUserTransactions (@RequestParam String username,
									 	@RequestParam String password) throws ResourceNotFoundException {
		
		User user = this.userController.getUserForTransaction(username, password, this.userRepository);
		 
		if (user == null)
			return ValidationsAndConstants.ERROR_MESSAGE_USER_NOT_EXIST;
		
		return user.getTransactions().toString();
		
	}
	
	@RequestMapping("/user/totalbalance")
	public @ResponseBody String getTotalBalance (@RequestParam String username,
									 	@RequestParam String password) throws ResourceNotFoundException {
		
		User user = this.userController.getUserForTransaction(username, password, this.userRepository);
		 
		if (user == null)
			return ValidationsAndConstants.ERROR_MESSAGE_USER_NOT_EXIST;
		
		long totalBalance = this.transactionController.getTotalBalance(user);
		
		return "Your total balance is " + totalBalance + ".";
		
	}
	
}
