package com.project.banking.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "transaction")
public class Transaction {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
	
    private String transactionType;
    
    private long amount;
    
    private String description;
    
    private LocalDate transactionDate;
    
    private User user;
 
    public Transaction() {
  
    }
    
    public Transaction(String transactionType, long amount, String description, LocalDate transactionDate) {
		super();
		this.transactionType = transactionType;
		this.amount = amount;
		this.description = description;
		this.transactionDate = transactionDate;
	}
 
    public Transaction(String transactionType, long amount, String description, User user) {
		super();
		this.transactionType = transactionType;
		this.amount = amount;
		this.description = description;
		this.user = user;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
        public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    
    @Column(name = "transaction_type", nullable = false)
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	@Column(name = "amount", nullable = false)
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}

	@Column(name = "description", nullable = false)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	@Column(name = "transaction_date", nullable = false)
	public LocalDate getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(LocalDate transactionDate) {
		this.transactionDate = transactionDate;
	}

	@Override
    public String toString() {
        return "transaction [id=" + id + ", transaction_type=" + transactionType + ", amount=" + amount + ", description=" + description
       + "]";
    }
 
}