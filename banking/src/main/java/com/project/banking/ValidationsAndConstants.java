package com.project.banking;

public class ValidationsAndConstants {

	public static final String TRANSACTION_TYPE_DEPOSIT = "deposit";
	public static final String TRANSACTION_TYPE_WITHDRAW = "withdraw";
	
	public static final String ERROR_MESSAGE_TRANSACTION = "transaction unsuccessful";
	public static final String ERROR_MESSAGE_NEGATIVE_AMOUNT = "amount should be positive";
	public static final String ERROR_MESSAGE_USER_NOT_EXIST = "user does not exist";
	public static final String ERROR_MESSAGE_WRONG_TRANS = "Wrong transaction type";
	
	public static final String SUCCESS_MESSAGE_TRANSACTION = "Successful transaction";
	
	
	public static String isValidTransaction(long amount)  {
		
		String error = null;
		
		if (amount < 0)
			error = ERROR_MESSAGE_NEGATIVE_AMOUNT + ".";
		
		return error;
		
	}

}
