package com.project.banking.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.project.banking.ValidationsAndConstants;

import com.project.banking.model.Transaction;
import com.project.banking.repository.TransactionRepository;

import com.project.banking.model.User;
import com.project.banking.repository.UserRepository;

public class TransactionController {
	
	Logger logger = LoggerFactory.getLogger(TransactionController.class);

	@Autowired
    private TransactionRepository transactionRepository;
	
    public List<Transaction> getAllUsers() {
        return transactionRepository.findAll();
    }

    public Transaction createTransaction(User user,
    									 String transactionType, 
    									 long amount,
    									 String description,
    									 UserRepository userRepository,
    									 TransactionRepository transactionRepository) throws ResourceNotFoundException {
    	
    	LocalDate transactionDate = LocalDate.now();
    	
    	Transaction transaction = new Transaction(transactionType, amount, description, transactionDate);

		if (transaction.getTransactionType() == ValidationsAndConstants.TRANSACTION_TYPE_DEPOSIT) {
			transaction.setUser(user);
    		return transactionRepository.save(transaction);
		}
		
		else if (transaction.getTransactionType() == ValidationsAndConstants.TRANSACTION_TYPE_WITHDRAW 
				&& this.isValidWithDraw(user.getTransactions(), amount)) {
			
			transaction.setUser(user);
			return transactionRepository.save(transaction);
		}
		
		else {
			return null;
		}
    	
        
    }
    
    private boolean isValidWithDraw (Set<Transaction> transactions, long amountWithdraw) {
    	
    	long positiveAmount = 0;
    	long negativeAmount = 0;
    	
    	for (Transaction transaction : transactions) {
    		
    		if (transaction.getTransactionType().equals(ValidationsAndConstants.TRANSACTION_TYPE_DEPOSIT)) 
    			positiveAmount = positiveAmount + transaction.getAmount();
    		else if (transaction.getTransactionType().equals(ValidationsAndConstants.TRANSACTION_TYPE_WITHDRAW)) 
    			negativeAmount = negativeAmount + transaction.getAmount();
    			
		}
    	
    	if (positiveAmount >= (negativeAmount + amountWithdraw)) 
    		return true;
    		
    	return false;
    }
    
    public long getTotalBalance (User user) {

    	long positiveAmount = 0;
    	long negativeAmount = 0;

    	Set<Transaction> transactions = user.getTransactions();

    	for (Transaction transaction : transactions) {

    		if (transaction.getTransactionType().equals(ValidationsAndConstants.TRANSACTION_TYPE_DEPOSIT)) 
    			positiveAmount = positiveAmount + transaction.getAmount();
    		else if (transaction.getTransactionType().equals(ValidationsAndConstants.TRANSACTION_TYPE_WITHDRAW)) 
    			negativeAmount = negativeAmount + transaction.getAmount();

    	}

    	return (positiveAmount - negativeAmount);

    }


}