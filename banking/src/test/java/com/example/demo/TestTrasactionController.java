package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.mock.MockTransactionRepository;
import com.example.demo.mock.MockUserRepository;
import com.project.banking.ValidationsAndConstants;
import com.project.banking.controller.ResourceNotFoundException;
import com.project.banking.controller.TransactionController;
import com.project.banking.controller.UserController;
import com.project.banking.model.Transaction;
import com.project.banking.model.User;
import com.project.banking.repository.TransactionRepository;
import com.project.banking.repository.UserRepository;

public class TestTrasactionController extends SpringBootTests  {

private User myUser;

private Transaction myTransaction;

private UserController userController = new UserController();

@Autowired
private UserRepository userRepository = new MockUserRepository();

private TransactionController transactionController = new TransactionController();

@Autowired
private TransactionRepository transactionRepository = new MockTransactionRepository();
	
	private void createTestUser() {
		
		this.myUser = new User();
		this.myUser.setEmail("1111@1111");
		this.myUser.setUsername("1111");
		this.myUser.setHash("12131321323211312321");
		this.myUser.setPassword("1111");
		
		Set<Transaction> transactions = new HashSet<Transaction>(); 
		
		this.myUser.setTransactions(transactions);
		
	}
	
	private Transaction createTestDepositTransaction() {
		crateTransaction(1);
		return this.myTransaction;
	}
	
	private Transaction createTestWithdrawTransaction() {
		crateTransaction(2);
		return this.myTransaction;
	}
	
	private void crateTransaction(int type) {
		
		this.myTransaction = new Transaction();
		if (myUser == null)
			this.createTestUser();
		this.myTransaction.setId(111);
		this.myTransaction.setTransactionDate(LocalDate.now());
		if (type == 1) {
			this.myTransaction.setTransactionType(ValidationsAndConstants.TRANSACTION_TYPE_DEPOSIT);
			this.myTransaction.setAmount(10);
		}
		else {
			this.myTransaction.setTransactionType(ValidationsAndConstants.TRANSACTION_TYPE_WITHDRAW);
			this.myTransaction.setAmount(5);
		}
		
		this.myTransaction.setDescription("11111");
	}
	
	@Test
	public void testCreateTransaction() {
		
		createTestUser();
		
		Transaction depositTransaction = createTestDepositTransaction();
		
		Transaction withdrawTransaction = createTestWithdrawTransaction();
		
		Transaction createdTransaction;
		
		try {
			/* Test Deposit Create Transaction*/
			createdTransaction = this.transactionController.createTransaction(this.myUser, 
																			  depositTransaction.getTransactionType(),
																			  depositTransaction.getAmount(),
																			  depositTransaction.getDescription(),
																			  this.userRepository,
																			  this.transactionRepository);
			if (createdTransaction != null)
				assertEquals(createdTransaction.getId(), depositTransaction.getId()); 
			else
				fail("Transaction null");
			
			/* Test Withdraw Create Transaction*/
			createdTransaction = this.transactionController.createTransaction(this.myUser, 
																			withdrawTransaction.getTransactionType(),
																			withdrawTransaction.getAmount(),
																			withdrawTransaction.getDescription(),
																			this.userRepository,
																			this.transactionRepository);
			if (createdTransaction != null)
				assertEquals(createdTransaction.getId(), withdrawTransaction.getId()); 
			else
				fail("Transaction null");
			
			
			
		} catch (ResourceNotFoundException e) {
			fail("ResourceNotFoundException found");
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void testCheckTransactions() {
		
		this.createTestUser();
		
		try {
			
			User user = this.userController.getUserForTransaction(this.myUser.getUsername(), this.myUser.getPassword(), this.userRepository);
		
			Set<Transaction> userTransactions = user.getTransactions();
			
			if (userTransactions != null)
				assertEquals(5, userTransactions.size());
			else
				fail("User have no transactions and was expecting 5 transactios");
			
			
		} catch (ResourceNotFoundException e) {
			
			fail("ResourceNotFoundException found");
			
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testGetTotalBalance() {
		
		this.createTestUser();
		
		try {
			
			User user = this.userController.getUserForTransaction(this.myUser.getUsername(), this.myUser.getPassword(), this.userRepository);
			long totalBalance = 0;
			totalBalance = this.transactionController.getTotalBalance(user);
			
			if (totalBalance > 0)
				assertEquals(10, totalBalance);
			else
				fail("totalBalance expected 10, current is " + totalBalance);
			
		} catch (ResourceNotFoundException e) {
			
			fail("ResourceNotFoundException found");
			
			e.printStackTrace();
		}
		
	}
	
	
	
}
