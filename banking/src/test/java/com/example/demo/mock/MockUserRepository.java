package com.example.demo.mock;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.project.banking.ValidationsAndConstants;
import com.project.banking.model.Transaction;
import com.project.banking.model.User;
import com.project.banking.repository.UserRepository;

public class MockUserRepository implements UserRepository  {
	
	private User myUser = null;
	
	private Optional<User> myOptionalUser = Optional.empty();
	
	public MockUserRepository () {
		
		this.createUser();
		
	}
	
	private void createUser() {
		
		this.myUser = new User();
		this.myUser.setEmail("1111@1111");
		this.myUser.setUsername("1111");
		this.myUser.setHash("12131321323211312321");
		this.myUser.setPassword("1111");
		this.myUser.setTransactions(this.createTransactios());
		
		this.myOptionalUser = Optional.of(this.myUser); 
	}
	
	private Set<Transaction> createTransactios() {
		
		Set<Transaction> transactions = new HashSet<Transaction>(); 
		Transaction transactionAux;
		
		for (int i = 0; i < 5; i++) {
			
			transactionAux = new Transaction();
			transactionAux.setTransactionDate(LocalDate.now());
			
			if ((i % 2) == 0) {
				transactionAux.setTransactionType(ValidationsAndConstants.TRANSACTION_TYPE_WITHDRAW);
				transactionAux.setAmount(10);
			}
			else {
				transactionAux.setTransactionType(ValidationsAndConstants.TRANSACTION_TYPE_DEPOSIT);
				transactionAux.setAmount(10);
			}
			
			transactionAux.setDescription("11111");
			
			transactions.add(transactionAux);
		} 
		
		return transactions;
	}
	
	
	public User findByUsername( String username) {
		
		return myUser;
	}
	
	public User findByUsernameAndPassword( String username, String password) {
		
		if (username.equals(this.myUser.getUsername()) && password.equals(this.myUser.getPassword()))
			return this.myUser;

		return null;
		
	}
	

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends User> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteInBatch(Iterable<User> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User getOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<User> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> S save(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<User> findById(Long id) {
		
		if (id == this.myUser.getId())
			return this.myOptionalUser;
		
		return null;
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(User entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends User> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends User> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends User> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}
	

}