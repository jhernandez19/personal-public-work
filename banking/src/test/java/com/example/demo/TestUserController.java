package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.mock.MockUserRepository;
import com.project.banking.ValidationsAndConstants;
import com.project.banking.controller.ResourceNotFoundException;
import com.project.banking.controller.UserController;
import com.project.banking.model.Transaction;
import com.project.banking.model.User;
import com.project.banking.repository.UserRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value=UserController.class)
public class TestUserController extends SpringBootTests {
	
	private User myUser;
	
	private Transaction myTransaction;
	
	private UserController userController = new UserController();
	
	@Autowired
	private UserRepository userRepository = new MockUserRepository();
	
	private void createUser() {
		
		this.myUser = new User();
		
		this.myUser.setId(111);
		this.myUser.setEmail("1111@1111");
		this.myUser.setUsername("1111");
		this.myUser.setHash("12131321323211312321");
		this.myUser.setPassword("1111");
		
		Set<Transaction> transactions = new HashSet<Transaction>(); 
		transactions.add(createTestTransaction());
		
		myUser.setTransactions(transactions);
		
	}
	
	private Transaction createTestTransaction() {
		
		this.myTransaction = new Transaction();
		this.myTransaction.setId(111);
		this.myTransaction.setTransactionDate(LocalDate.now());
		this.myTransaction.setTransactionType(ValidationsAndConstants.TRANSACTION_TYPE_DEPOSIT);
		this.myTransaction.setAmount(10);
		this.myTransaction.setDescription("11111");
		
		return this.myTransaction;
	}
	
	@Test
	public void testUserSignUp() {
		
		this.createUser();
		
		try {
			User user = this.userController.getUserForTransaction(myUser.getUsername(), myUser.getPassword(), this.userRepository);
			
			if (user != null)
				assertEquals(this.myUser.getId(), user.getId()); 
			else
				fail("Null user found");
			
			user = this.userController.getUserForTransaction("22222", "2222", this.userRepository);
			
			assertEquals(null,user);
			
			
		} catch (ResourceNotFoundException e) {
			
			fail("ResourceNotFoundException found");
			
			e.printStackTrace();
		}
		
		
	}
	

}
